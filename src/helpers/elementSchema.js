export const ELEMENTTYPE = {
    rows: 'rows',
    text: 'text',
    wys: 'wys',
    variable: 'variable',
    img: 'img',
    signature: 'signature',
    columns: 'columns',
    devider: 'devider',
    code: 'code',
    table: 'table'
}