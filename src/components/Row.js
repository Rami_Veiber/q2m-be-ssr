import React, { useContext } from "react";
import Column from './Column'
import {
    ELEMENTTYPE
} from "../helpers/elementSchema"
import { uid } from 'uid'
import {DataContext, DomContext} from '../App'


const Row = ({ row }) => {
    const contextData = useContext(DataContext);
    const _data = contextData

    if (row.type === ELEMENTTYPE.rows) {
        if (row?.loop && _data.hasOwnProperty(row?.loop)) {
            return _data[row?.loop]?.map((item, index) => (
                <div key={uid()} style={{ ...row.style }}>
                    {row.columns.map((column, index) => {
                        if (column) {
                            return <Column key={uid()} column={column} />
                        } else {
                            return <div key={uid()} />
                        }
                    }
                    )}
                </div>
            ))
        }

        else {
            return (
                <div style={{ ...row.style }}>
                    {row.columns.map((column, index) => {
                        if (column) {
                            return <Column key={uid()} column={column} />
                        } else {
                            return <div key={uid()} />
                        }
                    }
                    )}
                </div>
            )
        }

    } else {
        return <></>
    }
}


export default Row