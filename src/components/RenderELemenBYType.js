import React from 'react'
import Text from './Text'
import Variable from './Variable'
import Image from './Image'
import Signature from './Signature'
import WysText from './WysText'
import Devider from './Devider'
import Code from './Code'
import Table from './Table'
import { ELEMENTTYPE } from '../helpers/elementSchema'

const RenderELemenBYType = ({ element }) => {
    switch (element.type) {
        case ELEMENTTYPE.text:
            return <Text element={element} />
        case ELEMENTTYPE.wys:
            return <WysText element={element} />
        case ELEMENTTYPE.variable:
            return <Variable element={element} />
        case ELEMENTTYPE.img:
            return <Image element={element} />
        case ELEMENTTYPE.signature:
            return <Signature element={element} />
        case ELEMENTTYPE.devider:
            return <Devider element={element} />
        case ELEMENTTYPE.code:
            return <Code element={element} />
        case ELEMENTTYPE.table:
            return <Table element={element}/>
        default:
            return <div />
    }
}


export default RenderELemenBYType