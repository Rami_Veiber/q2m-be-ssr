import React, {useContext} from 'react';
 import {DataContext, DomContext} from '../App'


const data = {
    "id": 0,
    "isStateEditable": true,
    "name": "State",
    "value": {
        "_id": "609928e4295a050015e2379a",
        "name": "RENEWAL_SMART",
        "dom": {
            "body": {
                "type": "body",
                "rows": [
                    "12c203c40d4",
                    "67405646831",
                    "133df545da9",
                    "fdea247affa",
                    "87fa24a6bdb",
                    "ec071433df6",
                    "ab56607f031",
                    "d34002cd460",
                    "0aa04a0c592",
                    "426a444b152",
                    "53b7f89717e",
                    "4bf61f5f8d6",
                    "01c88740c90"
                ]
            },
            "rows": {
                "12c203c40d4": {
                    "id": "12c203c40d4",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "2c203c40d4d"
                    ]
                },
                "67405646831": {
                    "id": "67405646831",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "7405646831d"
                    ]
                },
                "133df545da9": {
                    "id": "133df545da9",
                    "type": "rows",
                    "loop": "products",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "33df545da9c"
                    ]
                },
                "fdea247affa": {
                    "id": "fdea247affa",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 1,
                    "columns": [
                        "dea247affa3"
                    ]
                },
                "87fa24a6bdb": {
                    "id": "87fa24a6bdb",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 2,
                    "columns": [
                        "7fa24a6bdb7",
                        "fa24a6bdb7a",
                        "a24a6bdb7a1"
                    ]
                },
                "ec071433df6": {
                    "id": "ec071433df6",
                    "type": "rows",
                    "style": {
                        "minHeight": "10px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "c071433df65"
                    ]
                },
                "ab56607f031": {
                    "id": "ab56607f031",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "b56607f0314"
                    ]
                },
                "d34002cd460": {
                    "id": "d34002cd460",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 3,
                    "columns": [
                        "34002cd460e",
                        "4002cd460e2",
                        "002cd460e29",
                        "02cd460e295"
                    ]
                },
                "0aa04a0c592": {
                    "id": "0aa04a0c592",
                    "type": "rows",
                    "style": {
                        "minHeight": "12px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "aa04a0c5921"
                    ]
                },
                "426a444b152": {
                    "id": "426a444b152",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "26a444b1524"
                    ]
                },
                "53b7f89717e": {
                    "id": "53b7f89717e",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 3,
                    "columns": [
                        "3b7f89717e7",
                        "b7f89717e75",
                        "7f89717e75c",
                        "f89717e75c8"
                    ]
                },
                "4bf61f5f8d6": {
                    "id": "4bf61f5f8d6",
                    "type": "rows",
                    "style": {
                        "minHeight": "12px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "bf61f5f8d60"
                    ]
                },
                "01c88740c90": {
                    "id": "01c88740c90",
                    "type": "rows",
                    "style": {
                        "minHeight": "55px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "row",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "numOfColumns": 0,
                    "columns": [
                        "1c88740c90c"
                    ]
                }
            },
            "columns": {
                "2c203c40d4d": {
                    "id": "2c203c40d4d",
                    "rowId": "12c203c40d4",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "c203c40d4d0",
                        "203c40d4d0f",
                        "03c40d4d0f4"
                    ]
                },
                "7405646831d": {
                    "id": "7405646831d",
                    "rowId": "67405646831",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "20%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "9507385f5e9",
                        "57bac64b3ea"
                    ]
                },
                "33df545da9c": {
                    "id": "33df545da9c",
                    "rowId": "133df545da9",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "b335cdc7eea",
                        "a168707654a"
                    ]
                },
                "dea247affa3": {
                    "id": "dea247affa3",
                    "rowId": "fdea247affa",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "30%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "f7c9e575fba",
                        "f61b87cc1d6"
                    ]
                },
                "7fa24a6bdb7": {
                    "id": "7fa24a6bdb7",
                    "rowId": "87fa24a6bdb",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "537f203ee55",
                        "e9a84967898",
                        "61bb9092412",
                        "5b8242e109e"
                    ]
                },
                "fa24a6bdb7a": {
                    "id": "fa24a6bdb7a",
                    "rowId": "87fa24a6bdb",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "7d45a9a8b65",
                        "8198d904be9",
                        "c862576c188",
                        "fce04f29626"
                    ]
                },
                "a24a6bdb7a1": {
                    "id": "a24a6bdb7a1",
                    "rowId": "87fa24a6bdb",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "5866882866b",
                        "22c88c818f7",
                        "725d5cf60ad"
                    ]
                },
                "c071433df65": {
                    "id": "c071433df65",
                    "rowId": "ec071433df6",
                    "type": "columns",
                    "style": {
                        "minHeight": "10px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "cec626d0f5f"
                    ]
                },
                "b56607f0314": {
                    "id": "b56607f0314",
                    "rowId": "ab56607f031",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "30%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "5ab321bd513",
                        "5e9f0d79d23",
                        "ff5e030d56b"
                    ]
                },
                "34002cd460e": {
                    "id": "34002cd460e",
                    "rowId": "d34002cd460",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "9a922d31c65",
                        "86daeaa231e"
                    ]
                },
                "4002cd460e2": {
                    "id": "4002cd460e2",
                    "rowId": "d34002cd460",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "0fa6c60f957",
                        "63d9214ff91"
                    ]
                },
                "002cd460e29": {
                    "id": "002cd460e29",
                    "rowId": "d34002cd460",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "9d91bc0f0b3",
                        "3a49a87cdb5"
                    ]
                },
                "02cd460e295": {
                    "id": "02cd460e295",
                    "rowId": "d34002cd460",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "b286b839718",
                        "8a422093640"
                    ]
                },
                "aa04a0c5921": {
                    "id": "aa04a0c5921",
                    "rowId": "0aa04a0c592",
                    "type": "columns",
                    "style": {
                        "minHeight": "10px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "c0630b48557"
                    ]
                },
                "26a444b1524": {
                    "id": "26a444b1524",
                    "rowId": "426a444b152",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "30%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "0c52a6d9b88",
                        "b72d1fb798d"
                    ]
                },
                "3b7f89717e7": {
                    "id": "3b7f89717e7",
                    "rowId": "53b7f89717e",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "be820d56174",
                        "8347a448929",
                        "7ad1965822d",
                        "4b4c071ff9b"
                    ]
                },
                "b7f89717e75": {
                    "id": "b7f89717e75",
                    "rowId": "53b7f89717e",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "18839a85df3",
                        "1d6c1b6f5c9",
                        "ce69c8599dd",
                        "6502b090025"
                    ]
                },
                "7f89717e75c": {
                    "id": "7f89717e75c",
                    "rowId": "53b7f89717e",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "8ba795ffc0a",
                        "fa19c8c0e49",
                        "1a2c0cd5317",
                        "b2933eab24b"
                    ]
                },
                "f89717e75c8": {
                    "id": "f89717e75c8",
                    "rowId": "53b7f89717e",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "d5e0c4641b4",
                        "e426842a00b"
                    ]
                },
                "bf61f5f8d60": {
                    "id": "bf61f5f8d60",
                    "rowId": "4bf61f5f8d6",
                    "type": "columns",
                    "style": {
                        "minHeight": "10px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "5c92e23724f"
                    ]
                },
                "1c88740c90c": {
                    "id": "1c88740c90c",
                    "rowId": "01c88740c90",
                    "type": "columns",
                    "style": {
                        "minHeight": "50px",
                        "width": "100%",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px",
                        "display": "flex",
                        "flexDirection": "column",
                        "justifyContent": "flex-start",
                        "alignItems": "flex-start",
                        "borderStyle": "solid",
                        "borderWidth": "0px"
                    },
                    "elements": [
                        "b75496e2bbe",
                        "2a87a9d48c0"
                    ]
                }
            },
            "elements": {
                "c203c40d4d0": {
                    "id": "c203c40d4d0",
                    "rowId": "12c203c40d4",
                    "columnId": "2c203c40d4d",
                    "type": "img",
                    "src": "https://zappa-mosh-social2.s3.eu-central-1.amazonaws.com/smartLogo.jpg",
                    "style": {
                        "width": "300px",
                        "height": "140px"
                    }
                },
                "203c40d4d0f": {
                    "id": "203c40d4d0f",
                    "rowId": "12c203c40d4",
                    "columnId": "2c203c40d4d",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "16px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Activation Date:",
                    "key": "date"
                },
                "03c40d4d0f4": {
                    "id": "03c40d4d0f4",
                    "rowId": "12c203c40d4",
                    "columnId": "2c203c40d4d",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "be8b5564d67": {
                    "id": "be8b5564d67",
                    "rowId": "3c40d4d0f41",
                    "columnId": "c40d4d0f41e",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Product",
                    "key": "label"
                },
                "8ae8a43b7fa": {
                    "id": "8ae8a43b7fa",
                    "rowId": "3c40d4d0f41",
                    "columnId": "c40d4d0f41e",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Price",
                    "key": "price"
                },
                "9507385f5e9": {
                    "id": "9507385f5e9",
                    "rowId": "67405646831",
                    "columnId": "7405646831d",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "0px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "0px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "ORDER ITEMS"
                },
                "57bac64b3ea": {
                    "id": "57bac64b3ea",
                    "rowId": "67405646831",
                    "columnId": "7405646831d",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "0px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "b335cdc7eea": {
                    "id": "b335cdc7eea",
                    "rowId": "133df545da9",
                    "columnId": "33df545da9c",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Product",
                    "key": "label"
                },
                "a168707654a": {
                    "id": "a168707654a",
                    "rowId": "133df545da9",
                    "columnId": "33df545da9c",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Price",
                    "key": "price"
                },
                "f7c9e575fba": {
                    "id": "f7c9e575fba",
                    "rowId": "fdea247affa",
                    "columnId": "dea247affa3",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "CHARGE SUMMARY"
                },
                "f61b87cc1d6": {
                    "id": "f61b87cc1d6",
                    "rowId": "fdea247affa",
                    "columnId": "dea247affa3",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "537f203ee55": {
                    "id": "537f203ee55",
                    "rowId": "87fa24a6bdb",
                    "columnId": "7fa24a6bdb7",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Charges Overview"
                },
                "e9a84967898": {
                    "id": "e9a84967898",
                    "rowId": "87fa24a6bdb",
                    "columnId": "7fa24a6bdb7",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "7d45a9a8b65": {
                    "id": "7d45a9a8b65",
                    "rowId": "87fa24a6bdb",
                    "columnId": "fa24a6bdb7a",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Bill Projection"
                },
                "8198d904be9": {
                    "id": "8198d904be9",
                    "rowId": "87fa24a6bdb",
                    "columnId": "fa24a6bdb7a",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "5866882866b": {
                    "id": "5866882866b",
                    "rowId": "87fa24a6bdb",
                    "columnId": "a24a6bdb7a1",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Pay Now"
                },
                "22c88c818f7": {
                    "id": "22c88c818f7",
                    "rowId": "87fa24a6bdb",
                    "columnId": "a24a6bdb7a1",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "61bb9092412": {
                    "id": "61bb9092412",
                    "rowId": "87fa24a6bdb",
                    "columnId": "7fa24a6bdb7",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Total Monthly Reccurent",
                    "key": "totalMonthlyRecurrent"
                },
                "c862576c188": {
                    "id": "c862576c188",
                    "rowId": "87fa24a6bdb",
                    "columnId": "fa24a6bdb7a",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Total Monthly Reccurent",
                    "key": "totalMonthlyRecurrent"
                },
                "725d5cf60ad": {
                    "id": "725d5cf60ad",
                    "rowId": "87fa24a6bdb",
                    "columnId": "a24a6bdb7a1",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "paymentNow"
                },
                "5b8242e109e": {
                    "id": "5b8242e109e",
                    "rowId": "87fa24a6bdb",
                    "columnId": "7fa24a6bdb7",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Tax",
                    "key": "tax"
                },
                "fce04f29626": {
                    "id": "fce04f29626",
                    "rowId": "87fa24a6bdb",
                    "columnId": "fa24a6bdb7a",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "100%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Tax",
                    "key": "tax"
                },
                "cec626d0f5f": {
                    "id": "cec626d0f5f",
                    "rowId": "ec071433df6",
                    "columnId": "c071433df65",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "5ab321bd513": {
                    "id": "5ab321bd513",
                    "rowId": "ab56607f031",
                    "columnId": "b56607f0314",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Customer Details"
                },
                "5e9f0d79d23": {
                    "id": "5e9f0d79d23",
                    "rowId": "ab56607f031",
                    "columnId": "b56607f0314",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "1699466ebcc": {
                    "id": "1699466ebcc",
                    "rowId": "07e8e19bd14",
                    "columnId": "5410ea9e15a",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Name"
                },
                "ff5e030d56b": {
                    "id": "ff5e030d56b",
                    "rowId": "ab56607f031",
                    "columnId": "b56607f0314",
                    "type": "text",
                    "style": {
                        "fontSize": "120%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Main Contact"
                },
                "fdda339bb6a": {
                    "id": "fdda339bb6a",
                    "rowId": "07e8e19bd14",
                    "columnId": "5410ea9e15a",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "120%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "fullName"
                },
                "b4df55460fa": {
                    "id": "b4df55460fa",
                    "rowId": "07e8e19bd14",
                    "columnId": "410ea9e15a0",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Email"
                },
                "5fd013f14c8": {
                    "id": "5fd013f14c8",
                    "rowId": "07e8e19bd14",
                    "columnId": "410ea9e15a0",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "email"
                },
                "8bee8441c99": {
                    "id": "8bee8441c99",
                    "rowId": "07e8e19bd14",
                    "columnId": "10ea9e15a02",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "Title",
                    "key": "empty"
                },
                "9a922d31c65": {
                    "id": "9a922d31c65",
                    "rowId": "d34002cd460",
                    "columnId": "34002cd460e",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Name"
                },
                "86daeaa231e": {
                    "id": "86daeaa231e",
                    "rowId": "d34002cd460",
                    "columnId": "34002cd460e",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "fullName"
                },
                "0fa6c60f957": {
                    "id": "0fa6c60f957",
                    "rowId": "d34002cd460",
                    "columnId": "4002cd460e2",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Email"
                },
                "63d9214ff91": {
                    "id": "63d9214ff91",
                    "rowId": "d34002cd460",
                    "columnId": "4002cd460e2",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "email"
                },
                "9d91bc0f0b3": {
                    "id": "9d91bc0f0b3",
                    "rowId": "d34002cd460",
                    "columnId": "002cd460e29",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Phone"
                },
                "3a49a87cdb5": {
                    "id": "3a49a87cdb5",
                    "rowId": "d34002cd460",
                    "columnId": "002cd460e29",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "phone"
                },
                "b286b839718": {
                    "id": "b286b839718",
                    "rowId": "d34002cd460",
                    "columnId": "02cd460e295",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Date Of Birthday"
                },
                "8a422093640": {
                    "id": "8a422093640",
                    "rowId": "d34002cd460",
                    "columnId": "02cd460e295",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "date"
                },
                "c0630b48557": {
                    "id": "c0630b48557",
                    "rowId": "0aa04a0c592",
                    "columnId": "aa04a0c5921",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "0c52a6d9b88": {
                    "id": "0c52a6d9b88",
                    "rowId": "426a444b152",
                    "columnId": "26a444b1524",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Account Details"
                },
                "b72d1fb798d": {
                    "id": "b72d1fb798d",
                    "rowId": "426a444b152",
                    "columnId": "26a444b1524",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "be820d56174": {
                    "id": "be820d56174",
                    "rowId": "53b7f89717e",
                    "columnId": "3b7f89717e7",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Account ID"
                },
                "8347a448929": {
                    "id": "8347a448929",
                    "rowId": "53b7f89717e",
                    "columnId": "3b7f89717e7",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "accountId"
                },
                "7ad1965822d": {
                    "id": "7ad1965822d",
                    "rowId": "53b7f89717e",
                    "columnId": "3b7f89717e7",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Tax Exemption"
                },
                "4b4c071ff9b": {
                    "id": "4b4c071ff9b",
                    "rowId": "53b7f89717e",
                    "columnId": "3b7f89717e7",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "taxExemption"
                },
                "18839a85df3": {
                    "id": "18839a85df3",
                    "rowId": "53b7f89717e",
                    "columnId": "b7f89717e75",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Account Type"
                },
                "1d6c1b6f5c9": {
                    "id": "1d6c1b6f5c9",
                    "rowId": "53b7f89717e",
                    "columnId": "b7f89717e75",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "accountType"
                },
                "ce69c8599dd": {
                    "id": "ce69c8599dd",
                    "rowId": "53b7f89717e",
                    "columnId": "b7f89717e75",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Billing Address"
                },
                "6502b090025": {
                    "id": "6502b090025",
                    "rowId": "53b7f89717e",
                    "columnId": "b7f89717e75",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "address"
                },
                "8ba795ffc0a": {
                    "id": "8ba795ffc0a",
                    "rowId": "53b7f89717e",
                    "columnId": "7f89717e75c",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Currency"
                },
                "fa19c8c0e49": {
                    "id": "fa19c8c0e49",
                    "rowId": "53b7f89717e",
                    "columnId": "7f89717e75c",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "currency"
                },
                "1a2c0cd5317": {
                    "id": "1a2c0cd5317",
                    "rowId": "53b7f89717e",
                    "columnId": "7f89717e75c",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Payment Method"
                },
                "b2933eab24b": {
                    "id": "b2933eab24b",
                    "rowId": "53b7f89717e",
                    "columnId": "7f89717e75c",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "rpm"
                },
                "d5e0c4641b4": {
                    "id": "d5e0c4641b4",
                    "rowId": "53b7f89717e",
                    "columnId": "f89717e75c8",
                    "type": "text",
                    "style": {
                        "fontSize": "150%",
                        "display": "flex",
                        "alignItems": "center",
                        "justifyContent": "center",
                        "padding": "0",
                        "margin": "0",
                        "width": "fit-content",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px",
                        "paddingTop": "2px",
                        "paddingBottom": "2px",
                        "paddingLeft": "2px",
                        "paddingRight": "2px"
                    },
                    "content": "Language"
                },
                "e426842a00b": {
                    "id": "e426842a00b",
                    "rowId": "53b7f89717e",
                    "columnId": "f89717e75c8",
                    "type": "variable",
                    "style": {
                        "title": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginLeft": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        },
                        "key": {
                            "fontSize": "150%",
                            "display": "flex",
                            "alignItems": "center",
                            "justifyContent": "center",
                            "marginLeft": "30px",
                            "fontWeight": "bolder",
                            "marginTop": "2px",
                            "marginBottom": "2px",
                            "marginRight": "2px",
                            "paddingTop": "2px",
                            "paddingBottom": "2px",
                            "paddingLeft": "2px",
                            "paddingRight": "2px"
                        }
                    },
                    "title": "",
                    "key": "language"
                },
                "5c92e23724f": {
                    "id": "5c92e23724f",
                    "rowId": "4bf61f5f8d6",
                    "columnId": "bf61f5f8d60",
                    "type": "devider",
                    "style": {
                        "width": "100%",
                        "height": "1px",
                        "backgroundColor": "#000000",
                        "marginTop": "2px",
                        "marginBottom": "2px",
                        "marginLeft": "2px",
                        "marginRight": "2px"
                    }
                },
                "b75496e2bbe": {
                    "id": "b75496e2bbe",
                    "rowId": "01c88740c90",
                    "columnId": "1c88740c90c",
                    "type": "wys",
                    "content": "<h5 style=\"text-align:start;\">My signature below signisies that:</h5>\n<h5><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">1. ACCEPTANCE THE USE OF LOREM IPSUM TERMS AND CONDITIONS</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">Your access to and use of Lorem Ipsum (the app) is subject exclusively to these Terms and Conditions. You will not use the app for any purpose that is unlawful or prohibited by these Terms and Conditions. By using the app you are fully accepting the terms, conditions and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the app.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">2. CREDIT CARD DETAILS</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">All Lorem Ipsum purchases are managed by the individual App Stores (Apple, Google Windows) and Lorem Ipsum will never store your credit card information or make it available to any third parties. Any purchasing information provided will be provided directly from you to the respective App Store and you will be subject to their credit card policies.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">3. LEGAL ADVICE</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">The contents of Lorem Ipsum app do not constitute advice and should not be relied upon in making or refraining from making, any decision.</span><br><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">All material contained on Lorem Ipsum is provided without any or warranty of any kind. You use the material on Lorem Ipsum at your own discretion</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">4. CHANGE OF USE</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">Lorem Ipsum reserves the right to:</span><br></p>\n<ul>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">4.1 change or remove (temporarily or permanently) the app or any part of it without notice and you confirm that Lorem Ipsum shall not be liable to you for any such change or removal and.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">4.2 change these Terms and Conditions at any time, and your continued use of the app following any changes shall be deemed to be your acceptance of such change.</span></li>\n</ul>\n<p style=\"text-align:start;\"><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">5. LINKS TO THIRD PARTY APPS AND WEBSITES</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">Lorem Ipsum app may include links to third party apps and websites that are controlled and maintained by others. Any link to other apps and websites is not an endorsement of such and you acknowledge and agree that we are not responsible for the content or availability of any such apps and websites.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">6. COPYRIGHT</span></h5>\n<p style=\"text-align:start;\"></p>\n<ul>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">6.1 All copyright, trade marks and all other intellectual property rights in the app and its content (including without limitation the app design, text, graphics and all software and source codes connected with the app) are owned by or licensed to Lorem Ipsum or otherwise used by Lorem Ipsum as permitted by law.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">6.2 In accessing the app you agree that you will access the content solely for your personal, non-commercial use. None of the content may be downloaded, copied, reproduced, transmitted, stored, sold or distributed without the prior written consent of the copyright holder. This excludes the downloading, copying and/or printing of pages of the app for personal, non-commercial home use only.</span></li>\n</ul>\n<p style=\"text-align:start;\"><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">7. LINKS TO AND FROM OTHER APPS AND WEBSITES</span></h5>\n<p style=\"text-align:start;\"></p>\n<ul>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">7.1 Throughout this app you may find links to third party apps. The provision of a link to such an app does not mean that we endorse that app. If you visit any app via a link in this app you do so at your own risk.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">7.2 Any party wishing to link to this app is entitled to do so provided that the conditions below are observed:</span></li>\n<li></li>\n</ul>\n<ol>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">(a) You do not seek to imply that we are endorsing the services or products of another party unless this has been agreed with us in writing;</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">(b) You do not misrepresent your relationship with this app; and</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">(c) The app from which you link to this app does not contain offensive or otherwise controversial content or, content that infringes any intellectual property rights or other rights of a third party.</span></li>\n</ol>\n<ul>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">7.3 By linking to this app in breach of our terms, you shall indemnify us for any loss or damage suffered to this app as a result of such linking.</span></li>\n</ul>\n<p style=\"text-align:start;\"><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">8. DISCLAIMERS AND LIMITATION OF LIABILITY</span></h5>\n<p style=\"text-align:start;\"></p>\n<ul>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">8.1 The app is provided on an AS IS and AS AVAILABLE basis without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">8.2 To the extent permitted by law, Lorem Ipsum will not be liable for any indirect or consequential loss or damage whatever (including without limitation loss of business, opportunity, data, profits) arising out of or in connection with the use of the app.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">8.3 Lorem Ipsum makes no warranty that the functionality of the app will be uninterrupted or error free, that defects will be corrected or that the app or the server that makes it available are free of viruses or anything else which may be harmful or destructive.</span></li>\n<li><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">8.4 Nothing in these Terms and Conditions shall be construed so as to exclude or limit the liability of Lorem Ipsum for death or personal injury as a result of the negligence of Lorem Ipsum or that of its employees or agents.</span></li>\n</ul>\n<p style=\"text-align:start;\"><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">9. INDEMNITY</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">You agree to indemnify and hold Lorem Ipsum and its employees and agents harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any claims or actions brought against Lorem Ipsum arising out of any breach by you of these Terms and Conditions or other liabilities arising out of your use of this app.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">10. SEVERANCE</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">If any of these Terms and Conditions should be determined to be invalid, illegal or unenforceable for any reason by any court of competent jurisdiction then such Term or Condition shall be severed and the remaining Terms and Conditions shall survive and remain in full force and effect and continue to be binding and enforceable.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">11. WAIVER</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">If you breach these Conditions of Use and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these Conditions of Use.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">12. GOVERNING LAW</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">These Terms and Conditions shall be governed by and construed in accordance with the law of and you hereby submit to the exclusive jurisdiction of the courts.</span><br></p>\n<h5 style=\"text-align:start;\"><span style=\"color: rgb(62,62,62);background-color: rgb(255,255,255);font-size: 13px;\">13. OUR CONTACT DETAILS</span></h5>\n<p style=\"text-align:start;\"><span style=\"color: rgb(114,114,114);background-color: rgb(255,255,255);font-size: 16px;font-family: Open Sans;\">Our Support Address:</span> <a href=\"http://www.astudioofourown.com/\" target=\"_blank\"><span style=\"color: rgb(41,128,185);background-color: initial;font-size: 16px;font-family: Open Sans;\">http://www.astudioofourown.com</span></a></p>\n"
                },
                "2a87a9d48c0": {
                    "id": "2a87a9d48c0",
                    "rowId": "01c88740c90",
                    "columnId": "1c88740c90c",
                    "type": "signature",
                    "style": {
                        "width": "160px",
                        "height": "80px",
                        "border": "2px solid",
                        "marginTop": "20px"
                    }
                }
            }
        },
        "date": "2021-05-10T12:36:52.036Z",
        "__v": 0
    },
    "subHooks": []
}

const Variable = ({ element }) => {
    const contextData = useContext(DataContext); 
    const domcontextData = useContext(DomContext);
    const _domStructure = domcontextData
    const _rows = _domStructure?.rows
    const _row = _rows[element.rowId]

    
    const valueIt = () => {
        switch (element.key) {
            case 'empty':
                return <span style={{ width: '50px', height: '1px', borderBottom: '1px solid black' }} />
            default: {
                if (_row?.loop) {
                    return contextData[_row?.loop][0][element.key]
                } else {
                    return contextData[element.key]
                }
            }
        }
    }

    return (
        <div style={{ display: "flex", alignItems: "flex-end", }}>
            <p style={{ ...element.style.title }} >{element.title}</p>
            <p style={{ ...element.style.key }}>{valueIt()}</p>
        </div>
    );
};

export default Variable;