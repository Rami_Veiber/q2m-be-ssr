import React, { useEffect, useState } from 'react';
import { uid } from 'uid'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const varJson = {
	"empty": "empty",
	"userName": "Csr Agent",
	"fullName": "Estban Fernandez Gonzles",
	"address": "Estaka Mabini, street - Dondla, zip - 123342,  Philiphine",
	"accountBalance": "PHP 234.556",
	"paymentNow": "PHP 1000",
	"product": "Iphone 12 Black 128 Giga",
	"storeName": "Super Phones",
	"agentName": "Philipe Lopez to",
	"phone": "0542291101",
	"products": [
		{
			"label": "Iphone 12 Black 128 Giga",
			"price": "PHP 1000"
		},
		{
			"label": "Iphone 12 Gold 256 Giga",
			"price": "PHP 1800"
		}
	],
	"sellsInfo": {
		"header": [
			{
				"title": "Product"
			},
			{
				"title": "Date"
			},
			{
				"title": "Price"
			}
		],
		"body": [
			{
				"product": "Ipone",
				"date": "12/6/2020",
				"price": "PHP 2000"
			}
		]
	}
}


const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});


const Header = ({ table, headerData }) => {
    const [data, setData] = useState(table?.headerContent)

    useEffect(() => {
        if (headerData?.length) {
            setData([...headerData])
        }
    }, [headerData])

    return (
        < TableHead >
            <TableRow>
                {
                    data.map((head, index) => {
                        return (
                            <TableCell key={uid()} >{head.title}</TableCell>
                        )
                    })
                }
            </TableRow>
        </TableHead >
    )

}

const Body = ({ table, bodyData }) => {
    const [data, setData] = useState(table?.headerContent)

    useEffect(() => {
        if (bodyData?.length) {
            setData([...bodyData])
        }
    }, [bodyData])

    return (
        <TableBody>
            {data.map((row, index) => {
                let toArrData = Object.keys(row)
                return <TableRow key={uid()} >
                    {
                        toArrData.map((cell, index) => {
                            return (
                                <TableCell key={uid()} >
                                    {row[cell]}
                                </TableCell>
                            )
                        })
                    }
                </TableRow>
            })}
        </TableBody>
    )
}


const PrintTable = ({ element }) => {
    const classes = useStyles();
    const _table = element?.table
    const data = varJson[element.loop]
    return (
        <div style={{ width: "100%" }}>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <Header table={_table} headerData={data?.header} />
                    <Body table={_table} bodyData={data?.body} />
                </Table>
            </TableContainer>

        </div>
    );
};

export default PrintTable;