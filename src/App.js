import React from 'react'
import './App.css';
import { uid } from 'uid'
import Row from './components/Row'

// const varJson = {
//   "empty": "empty",
//   "userName": "Csr Agent",
//   "fullName": "Estban Fernandez Gonzles",
//   "address": "Estaka Mabini, street - Dondla, zip - 123342,  Philiphine",
//   "accountBalance": "PHP 234.556",
//   "paymentNow": "PHP 1000",
//   "product": "Iphone 12 Black 128 Giga",
//   "storeName": "Super Phones",
//   "agentName": "Philipe Lopez to",
//   "phone": "0542291101",
//   "products": [
//       {
//           "label": "Iphone 12 Black 128 Giga",
//           "price": "PHP 1000"
//       },
//       {
//           "label": "Iphone 12 Gold 256 Giga",
//           "price": "PHP 1800"
//       }
//   ],
//   "sellsInfo": {
//       "header": [
//           {
//               "title": "Product"
//           },
//           {
//               "title": "Date"
//           },
//           {
//               "title": "Price"
//           }
//       ],
//       "body": [
//           {
//               "product": "Ipone",
//               "date": "12/6/2020",
//               "price": "PHP 2000"
//           }
//       ]
//   }
// }

export const DataContext = React.createContext();
export const DomContext = React.createContext();

function App({ document, varJson }) {
  const _domStructure = document?.dom
  const _rows = _domStructure?.rows
  const _columns = _domStructure?.columns
  const _elements = _domStructure?.elements

  // desctructure row
  let vdomRows = [..._domStructure?.body?.rows?.map(rowId => ({ ..._rows[rowId] }))]
  // desctructure column
  vdomRows.forEach(row => {
    row.columns = row.columns.map(columnId => {
      return _columns[columnId]
    })
  })

  vdomRows.forEach(row => {
    row.columns?.forEach(column => {
      if (column?.elements) {
        column.elements = column.elements.map(elementOrId => {
          if (typeof elementOrId === 'string') {
            return _elements[elementOrId]
          }
          else return _elements[elementOrId.id]
        })
      }
    });
  })

  return (
    <DataContext.Provider value={varJson}> 
      <DomContext.Provider value={_domStructure}>
        <div>
          {
            vdomRows.map((row, index) => <Row key={uid()} row={row} />)
          }
        </div>
      </DomContext.Provider>
    </DataContext.Provider>
  );
}

export default App;
