
const router = require('express').Router()
const DomHtml = require('../models/DomHtml')
import htmlBuilder from '../views/htmlBuilder'

router.post('/byname/', async (req, res) => {
    
    const document = await DomHtml.find({ name: req.body.name })
    let repsonse;
    
    if (document.length) {
        repsonse = htmlBuilder(document[0], req.body?.varJson)
    }
    // repsonse = htmlBuilder()
    return res.send(repsonse)
})

export default router
