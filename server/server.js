import express from "express";
import dotenv from 'dotenv'
import mongoose from 'mongoose'
import cors from 'cors'
import authRoute from './routes/auth'
import domBuilder from './routes/domRoute'
import htmlGenerator from './routes/htmlGenerator'

const port = process.env.PORT || 4001;

const app = express();


dotenv.config()

mongoose.connect(
    process.env.DATA_CONNECTED,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => console.log("Db connected"))

app.use(express.json())
app.use(cors())

app.use('/api/user', authRoute)
app.use('/api/dom', domBuilder)
app.use('/api/htmlgenerator', htmlGenerator)
app.use(express.static(__dirname + '/build'));
app.listen(port, () => {
    console.log(`App launched on ${port}`);
});