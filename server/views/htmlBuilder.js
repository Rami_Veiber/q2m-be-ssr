import React from 'react'
import App from '../../src/App'
import ReactDOMServer from 'react-dom/server'

const buildIndexHtml = (dom, title) => (
    `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <style>
       
        .MuiTableContainer-root {
            width: 100%;
            overflow-x: auto;
        }

        .MuiPaper-rounded {
            border-radius: 4px;
        }

        .makeStyles-table-66 {
            min-width: 650px;
        }

        .MuiTable-root {
            width: 100%;
            display: table;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .MuiTableHead-root {
            display: table-header-group;
        }

        .MuiTableRow-root {
            color: inherit;
            display: table-row;
            outline: 0;
            vertical-align: middle;
        }

        .MuiTableCell-head {
            color: rgba(0, 0, 0, 0.87);
            font-weight: 500;
            line-height: 1.5rem;
        }

        .MuiTableCell-root {
            display: table-cell;
            padding: 16px;
            font-size: 0.875rem;
            text-align: left;
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
            border-bottom: 1px solid rgba(224, 224, 224, 1);
            letter-spacing: 0.01071em;
            vertical-align: inherit;
        }

        .MuiTableBody-root {
            display: table-row-group;
        }

        .MuiTableRow-root {
            color: inherit;
            display: table-row;
            outline: 0;
            vertical-align: middle;
        }

        .MuiTableCell-root {
            display: table-cell;
            padding: 16px;
            font-size: 0.875rem;
            text-align: left;
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
            font-weight: 400;
            line-height: 1.43;
            border-bottom: 1px solid rgba(224, 224, 224, 1);
            letter-spacing: 0.01071em;
            vertical-align: inherit;
        }

        .MuiTableCell-body {
            color: rgba(0, 0, 0, 0.87);
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <title>${title}</title>
    </head>
    <body><div style="padding: 15px;">${dom}</div><body></html>`
    
)

const htmlBuilder = (document, varJson) => {
    let dom = ReactDOMServer.renderToStaticMarkup(<App document={document} varJson={varJson} />)
    let response = buildIndexHtml(dom, document?.name)
    return response
}

export default htmlBuilder

